from selenium import webdriver
import time
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

def download(type):
    driver.find_element_by_xpath('//*[@id="downloadArchive"]/table/tbody/tr[1]/td[5]/a').click()
    #move downloaded file
    import os
    import shutil
    downloadDir = 'c:\\users\\javier\\downloads\\'
    filenamesFull = []
    import time
    time.sleep(5) #await download completion
    for filename in os.listdir(downloadDir):
        if(filename.startswith('1') and filename.endswith('txt') and os.path.isfile(os.path.join(downloadDir, filename))):
            filenamesFull.append(os.path.join(downloadDir, filename))
    if(0 == len(filenamesFull)):
        print('No files found matching ' + downloadDir + '1*.txt')
        return False
    fromFilename = max(filenamesFull, key=os.path.getctime)
    downloadedFilename = os.path.join(downloadDir, fromFilename)
    import datetime
    today = datetime.datetime.now()
    dateToFilename = '{:%#m.%#d.%y}'.format(today)
    toFilename = type + '-' + dateToFilename + '.txt'
    targetFilenameWithPath = 'P:\\eCommerce\\Customers\\Amazon\\JMJD\\JMJ Sales Reports\\Reserve and FBA Inv\\' + toFilename
    shutil.move(downloadedFilename, targetFilenameWithPath)
    return True

driver = webdriver.Edge()
driver.implicitly_wait(10)
from selenium.webdriver.support.wait import WebDriverWait
wait_time_short = 10
wait_time_long = 100
wait_short = WebDriverWait(driver, wait_time_short)
wait_long = WebDriverWait(driver, wait_time_long)

#Reserve Inventory
target_url = 'https://sellercentral.amazon.com/gp/ssof/reports/search.html#orderAscending=&recordType=ReserveBreakdown&noResultType=&merchantSku=&fnSku=&FnSkuXORMSku=&reimbursementId=&orderId=&genericOrderId=&asin=&lpn=&shipmentId=&problemType=ALL_DEFECT_TYPES&hazmatStatus=&inventoryEventTransactionType=&fulfillmentCenterId=&transactionItemId=&inventoryAdjustmentReasonGroup=&eventDateOption=1&fromDate=mm%2Fdd%2Fyyyy&toDate=mm%2Fdd%2Fyyyy&startDate=&endDate=&fromMonth=1&fromYear=2019&toMonth=1&toYear=2019&startMonth=&startYear=&endMonth=&endYear=&specificMonth=1&specificYear=2019'
#todo use pathlib to handle Mac file paths' slashes
#todo configure the source of credentials
if target_url != driver.current_url :
    #todo: fetch credentials
    #todo: login
    driver.get(target_url)

#click 'Request .txt Download'
wait_short.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-md-sec')))
element = driver.find_element_by_class_name('btn-md-sec')
element.click()
#await, click 'Download'
try:
    wait_short.until(EC.text_to_be_present_in_element((By.XPATH, '//*[@id="downloadArchive"]/table/tbody/tr[1]/td[5]'), 'In Progress'))
except TimeoutException as e:
    print('No "In Progress" status after clicking "Request TXT" for "Reserved Inventory".')
try:
    wait_long.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="downloadArchive"]/table/tbody/tr[1]/td[5]/a')))
except TimeoutException as e:
    print('No Download link showed after ' + wait_time_long + 's.')

download('RESERVE')

#download FBA inventory
driver.find_element_by_link_text('Show more...').click()
driver.find_element_by_link_text('Manage FBA Inventory').click()
time.sleep(5)
wait_long.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="requestCsvTsvDownload"]/tr[1]/td[3]/button')))
time.sleep(5)
driver.find_element_by_xpath('//*[@id="requestCsvTsvDownload"]/tr[1]/td[3]/button').click()
try:
    wait_short.until(EC.text_to_be_present_in_element((By.XPATH, '//*[@id="downloadArchive"]/table/tbody/tr[1]/td[5]'), 'In Progress'))
except TimeoutException as e:
    print('No "In Progress" status after clicking "Request TXT" for "Manage FBA Inventory".')
try:
    wait_long.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="downloadArchive"]/table/tbody/tr[1]/td[5]/a')))
except TimeoutException as e:
    print('No Download link showed after ' + wait_time_long + 's.')
download('FBA')

driver.close()