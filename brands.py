#input: xlsx whose column are Walmart product' urls
#output: append new column each with respective brand

from selenium import webdriver
from openpyxl import load_workbook
import os.path

#get configuration from file
#    assign using whitelist method
import json
with open('config.json') as json_file:  
    config = json.load(json_file)
source_filename = config.get('source_filename', os.path.join(os.path.expanduser('~'), 'Downloads', 'urls.xlsx'))
if os.path.isfile(source_filename):
    fileOpen = open(source_filename)
else:
    print('Error: neither source file "' + config.get('source_filename') + '" nor default exist.')
    exit()

min_row = config.get('min_row', 2)
max_row = config.get('max_row', 3)
min_col = config.get('min_col', 1)
max_col = config.get('max_col', 1)

driver = webdriver.Edge()
driver.implicitly_wait(4)

import time
#read, follow links
wb = load_workbook(source_filename)
ws = wb.active
current_row = min_row
for row in ws.iter_rows(min_col=min_col, max_col=max_col, min_row=min_row, max_row=max_row, values_only=True):
    driver.get(row[0])
    time.sleep(3)
    #get brand
    brand = driver.find_element_by_xpath('/html/body/div[1]/div/div/div[2]/div/div[1]/div/div[1]/div/div/div/div/div[3]/div[4]/div[2]/div[1]/div/div/div/div[3]/div/div[2]/a/span')
    ws.cell(column=3, row=current_row, value=brand.text)
    current_row += 1

driver.close()
wb.save(source_filename)

print('Brands for rows ' + str(min_row) + '-' + str(current_row-1) + ' found and written.')
