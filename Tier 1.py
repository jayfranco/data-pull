from selenium import webdriver
import re

#@todo use explicit waits; however, unknown end condition
def download():
    driver.find_element_by_xpath('//*[@id="root"]/div/div/div[1]/div[2]/div[5]/div[1]/div[2]/div/div[1]/awsui-button-dropdown/div/button').click()
    driver.find_element_by_xpath('//*[@id="root"]/div/div/div[1]/div[2]/div[5]/div[1]/div[2]/div/div[1]/awsui-button-dropdown/div/div/ul/li/ul/li[1]/a').click()
    time.sleep(25)

driver = webdriver.Edge()
driver.implicitly_wait(13)

#get credentials
#@todo use pathlib to handle Mac file paths' slashes
import os
import time

#@todo configure the source of credentials
driver.get('https://vendorcentral.amazon.com/analytics/dashboard/salesDiagnostic')
if 'https://vendorcentral.amazon.com/analytics/dashboard/salesDiagnostic' != driver.current_url :
    file_credentials = os.path.join(os.path.expanduser('~'), 'Documents', 'credentials.txt')
    if os.path.isfile(file_credentials):
        fileOpen = open(file_credentials);
        fileLines = fileOpen.readlines();
        for line in fileLines:
            #print(line)
            username = ''
            password = ''
    else:
        print("Error: credentials file doesn't exist")
        exit()
        driver.find_element_by_id('ap_email').send_keys(username)
        driver.find_element_by_id('ap_password').send_keys(password)
        driver.find_element_by_id('signInSubmit').click()
        driver.get('https://vendorcentral.amazon.com/analytics/dashboard/salesDiagnostic')

#get ARA tier (Basic, Premium, other) from logo
ara_tier = re.search(r"Amazon Retail Analytics ([A-Za-z]+)", driver.find_element_by_xpath('//*[@id="root"]/div/div/div[1]/div[1]/div[1]/img').get_attribute('title')).group(1)

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

if 'Basic' == ara_tier:
    xpath = '//*[@id="root"]/div/div/div[1]/div[2]/div[5]/div[2]/div/div[1]/span[2]/div/awsui-button-dropdown/div/button/span[1]/span/span/strong'
elif 'Premium' == ara_tier:
    xpath = '//*[@id="root"]/div/div/div[1]/div[2]/div[5]/div[2]/div/div[1]/span[3]/div/awsui-button-dropdown/div/button/span[1]/span/span/strong'
    WebDriverWait(driver, 13).until( EC.element_to_be_clickable((By.XPATH, xpath )) )
element = driver.find_element_by_xpath(xpath)
if 'Shipped COGS' != element.text : 
    element.click()
    driver.find_element_by_partial_link_text('Shipped COGS').click()
xpath = '//*[@id="root"]/div/div/div[1]/div[2]/div[5]/div[2]/div/div[3]/span[1]/div/awsui-button-dropdown/div/button/span[1]/span/span/strong'
if 'Weekly' != driver.find_element_by_xpath(xpath).text : 
    driver.find_element_by_xpath(xpath).click()
    driver.find_element_by_partial_link_text('Weekly').click()
if 'Basic' == ara_tier:
    xpath = '//*[@id="root"]/div/div/div[1]/div[2]/div[5]/div[2]/div/div[3]/span[2]/div/awsui-button-dropdown/div/button/span[1]/span/span/strong'
    if 'Last reported' not in driver.find_element_by_xpath(xpath).text : 
        driver.find_element_by_xpath(xpath).click()
        driver.find_element_by_partial_link_text('Last reported').click()
if 'Basic' == ara_tier:
    pageDate = driver.find_element_by_xpath('//*[@id="root"]/div/div/div[1]/div[2]/div[5]/div[2]/div/div[3]/span[2]/div/awsui-button-dropdown/div/button/span[1]/span/span/strong').text
elif 'Premium' == ara_tier:
    pageDate = driver.find_element_by_xpath('//*[@id="root"]/div/div/div[1]/div[2]/div[5]/div[2]/div/div[3]/span[2]/div/div/div[1]/input').get_attribute('value')
dst_filename_date_re = re.search(r"\d{1,4}\/\d{1,4}\/\d{1,4}", pageDate)

download()

#todo: wait for the download to complete
driver.get('https://vendorcentral.amazon.com/analytics/dashboard/inventoryHealth')
xpath = '//*[@id="root"]/div/div/div[1]/div[2]/div[5]/div[2]/div/div[3]/span[1]/div/awsui-button-dropdown/div/button/span[1]/span/span/strong' #Reporting Range's value
WebDriverWait(driver, 13).until( EC.element_to_be_clickable((By.XPATH, xpath )) )
element = driver.find_element_by_xpath(xpath)
if 'Weekly' != element.text :
    element.click()
    driver.find_element_by_partial_link_text('Weekly').click()
if 'Basic' == ara_tier:
    xpath = '//*[@id="root"]/div/div/div[1]/div[2]/div[5]/div[2]/div/div[3]/span[2]/div/awsui-button-dropdown/div/button/span[1]/span/span/strong'
    if 'Last reported' not in driver.find_element_by_xpath(xpath).text : 
        driver.find_element_by_xpath(xpath).click()
        driver.find_element_by_partial_link_text('Last reported').click()
download()

driver.get('https://vendorcentral.amazon.com/analytics/dashboard/productCatalog')
xpath = '//*[@id="root"]/div/div/div[1]/div[2]/div[5]/div[2]/div/div[1]/span[1]/div/awsui-button-dropdown/div/button/span[1]/span/span/strong'
WebDriverWait(driver, 13).until( EC.element_to_be_clickable((By.XPATH, xpath )) )
element = driver.find_element_by_xpath(xpath)
if 'Manufacturing' != element.text : 
    element.click()
    driver.find_element_by_link_text('Manufacturing').click()
download()

import datetime
dst_filename_date = datetime.datetime.strptime(dst_filename_date_re.group(), '%m/%d/%y').strftime('%Y-%m-%d')

driver.close()

import shutil

shutil.move('C:\\users\\javier\\Downloads\\Sales Diagnostic_Summary View_US.xlsx', 'P:\\eCommerce\\Planning\\Query Repository\\Tier 1\\Weekly Sales Diagnostic_Detailed View - Repository\\Amazon US Weekly Sales Diagnostic_' + dst_filename_date + '.xlsx')
shutil.move('C:\\users\\javier\\Downloads\\Inventory Health_US.xlsx', 'P:\\eCommerce\\Planning\\Query Repository\\Tier 1\\Inventory Health_US - Tier 1.xlsx')
shutil.move('C:\\users\\javier\\Downloads\\Product Catalog_US.xlsx', 'P:\\eCommerce\\Planning\\Query Repository\\Tier 1\\Amazon Product Catalog_US - Tier 1.xlsx')
