# General Script Notes
Runs scheduled on a Windows machine so neither dev-env nor Mac equivalents, yet.

## Installation Notes
1. Install [choco](https://chocolatey.org/install) or homebrew
    * confirm [PowerShell](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-windows-powershell?view=powershell-6) already installed
2. Install Python
```sh
#Win
choco install python -y

#Mac
homebrew install python

#both
python -m pip install -U selenium
```
3. Save [Edge webdriver](https://developer.microsoft.com/en-us/microsoft-edge/tools/webdrivers/) to a `$PATH` directory
4. (Win only) Copy script to local (Task Scheduler only runs/views local files)
5. Run needed .py
```
#Win
"P:\eCommerce\Planning\Automation\Tier 1.py"

#Mac
python /Volumes/Public/eCommerce/Planning/Automation/Tier 1.py"
```
## Todo
* Mac version
* Edit config.json for your particular .py script
* automate it all

## Reference
1. Make these edits to `.py`'s if using Chrome
```js
options = webdriver.ChromeOptions()
options.add_argument('headless')
driver = webdriver.Chrome(options=options)
```
1. Save ChromeDriver to a `$PATH` directory

## Script: Download Tier 1
* Reference: XPaths for download links
```xpath
sales diagnostic
//*[@id="root"]/div/div/div[1]/div[2]/div[5]/div[1]/div[2]/div/div[1]/awsui-button-dropdown/div/div/ul/li[1]/ul/li[1]/a
inventory health
//*[@id="root"]/div/div/div[1]/div[2]/div[5]/div[1]/div[2]/div/div[1]/awsui-button-dropdown/div/div/ul/li/ul/li[1]/a
product catalog
//*[@id="root"]/div/div/div[1]/div[2]/div[5]/div[1]/div[2]/div/div[1]/awsui-button-dropdown/div/div/ul/li/ul/li[1]/a
```
* Reference
    * [EdgeDriver](https://docs.microsoft.com/en-us/microsoft-edge/webdriver#using-webdriver)
    * [Selenium](https://selenium-python.readthedocs.io/)
        * [API](https://selenium-python.readthedocs.io/api.html)
        * [selectors](https://selenium-python.readthedocs.io/locating-elements.html)
        * [sftptime](https://docs.python.org/2/library/datetime.html?highlight=strftime#strftime-and-strptime-behavior)
* Filename formats
```diff
from
Last reported (6/16/19 - 6/23/19)

to (if ARA Premium)
Amazon US Weekly Sales Diagnostic_2019-06-16.xlsx

or (if ARA Basic)
BasicA US Weekly Sales Diagnostic_2019-06-16.xlsx
```

## Script: Write Brands of ASINs
One time run for Michael but saved as informative for future tasks.

### Run
```sh
docker exec -it brands bash
pip install openpyxl
python brands.py
```

## Script: Business Reports
No notes.

## Script: FBA Inv and Reserve
No notes.
