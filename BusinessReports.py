from selenium import webdriver

driver = webdriver.Edge()
driver.implicitly_wait(130)

#construct dates needed
import datetime
from dateutil import relativedelta
today = datetime.datetime.now()
start = today - datetime.timedelta((today.weekday() + 1) % 7)
fromDate = start + relativedelta.relativedelta(weekday=relativedelta.SU(-2))
toDate = start + relativedelta.relativedelta(weekday=relativedelta.SA(-1))
fromDateStr = '{:%m/%d/%Y}'.format(fromDate)
toDateStr = '{:%m/%d/%Y}'.format(toDate)

target_url = 'https://sellercentral.amazon.com/gp/site-metrics/report.html#&cols=/c0/c1/c2/c3/c4/c5/c6/c7/c8/c9/c10/c11&sortColumn=12&filterFromDate='+fromDateStr+'&filterToDate='+toDateStr+'&fromDate='+fromDateStr+'&toDate='+toDateStr+'&reportID=102:DetailSalesTrafficByChildItem&sortIsAscending=0&currentPage=0&dateUnit=1&viewDateUnits=ALL&runDate='

#todo: use pathlib to handle Mac file paths' slashes

#todo: configure the source of credentials
if target_url != driver.current_url :
    #fetch credentials
    #login
    driver.get(target_url)
    
#check dates
assert (fromDateStr == driver.find_element_by_id('fromDate2').get_attribute('value')), 'wrong fromDate'
assert (toDateStr == driver.find_element_by_id('toDate2').get_attribute('value')), 'wrong toDate'

element = driver.find_element_by_class_name('actionsArrow')
element.click()
element = driver.find_element_by_id('downloadCSV')
element.click()

#todo: command argument of current date override
#move file; selenium can't control dialog boxes so only save to default then move
fromFileNameDate = datetime.datetime.now().strftime('%#m-%#d-%y')
toFileNameFromDate = today + relativedelta.relativedelta(weekday=relativedelta.SU(-2))
toFileNameToDate = today + relativedelta.relativedelta(weekday=relativedelta.SA(-1))

toFileNameDates = fromDate.strftime('%m.%d.%y') + '-' + toDate.strftime('%m.%d.%y')
fromFileName = 'C:\\users\\javier\\Downloads\\BusinessReport-' + today.strftime('%#m-%d-%y') + '.csv'
toFileName = 'P:\\eCommerce\\Customers\\Amazon\\JMJD\\JMJ Sales Reports\BusinessReport- ' + toFileNameDates + '.csv'
import shutil
shutil.move(fromFileName, toFileName)

driver.close()
